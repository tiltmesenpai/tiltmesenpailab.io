---
title: Resume
permalink: false
layout: resume.hbs
---

# Experience
## Security Engineer, AWS Lambda
#### July 2020 - Present
Contributed security domain expertise, drafted security-related customer messaging
and managed organization-wide security campaigns.

## Embedded Security Engineer, Amazon Prime Air
#### Aug 2019 - July 2020
Managed security assessments for hardware and firmware components at Amazon Prime Air.

## Security Consultant, Deja vu Security
#### Jan 2019-Aug 2019
Conducted security assessments of products for major tech companies, conducted internal
research and lead development of internal hardware training processes and standards.
Organized and managed bi-weekly hardware research and training sessions.  
Focuses in:
- Embedded Hardware Security
- Serverless Application Technologies
- Single Page Web Application Security

## Associate Security Consultant, Deja vu Security
#### 2017-2018
Conducted security assessments of products for major tech companies.  
Focuses in:
- Single Page Web Application Security
- REST API Security

## Intern, Cigital
#### Summer, 2014
Assisted in security assessments and developed internal tooling.  
Focuses in:
- Web application test automation
- Mobile Security (Android/iOS)

# Skills
- Embedded Security Assessment
    - Bus protocol analysis (I2C, SPI, LPC, etc)
    - ARM assembly and CPU configuration (Cortex M/A)
    - Trusted Enclave Architectures
        - ARM TrustZone
        - Intel SGX
- Mobile Security Assessment
    - Dynamic analysis (Frida)
        - iOS
        - Android
    - Disassembly/Reverse Engineering
        - Android
- Programming
    - Python, Java, C, Rust, Kotlin, Assembly (AArch64, AArch32, x86)
- Operating System Internals
    - Linux (Debian based), Android, ChromeOS, iOS