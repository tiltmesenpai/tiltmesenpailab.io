var Metalsmith  = require('metalsmith');
var drafts      = require('@metalsmith/drafts')
var collections = require('metalsmith-collections');
var copy        = require('metalsmith-copy');
var layouts     = require('metalsmith-layouts');
var md          = require('@metalsmith/markdown');
var partials    = require('metalsmith-discover-partials');
var permalinks  = require('@metalsmith/permalinks');
var sass        = require('metalsmith-sass');

var markdown    = md({
    gfm: true,
    tables: true,
    highlight: (code) => {
      return require('highlight.js').highlightAuto(code).value;
    }
  })

var site = Metalsmith(__dirname)
  .metadata({                 
    sitename: "Joshua Koike",
    siteurl: "https://tilting.me/",
    description: "Hello world",
    generatorname: "Metalsmith",
    generatorurl: "http://metalsmith.io/"
  })
  .source('./src')            // source directory
  .destination('./build')     // destination directory
  .clean(true)                // clean destination before
  .use(partials({
    directory: 'partials',
    pattern: /\.hbs$/
  }))
  .use(drafts())              // omit draft articles
  .use(collections({          // group all blog posts by internally
    posts: {
      pattern: 'posts/*.md',  // adding key 'collections':'posts'
      reverse: true
    }
  }))                         // use `collections.posts` in layouts
  .use(markdown)              // transpile all md into html
  .use(permalinks({           // change URLs to permalink URLs
    relative: false,          // put css only in /css
    // original options would act as the keys of a `default` linkset,
    pattern: ':date/:title',
    date: 'YYYY',
    // each linkset defines a match, and any other desired option
    linksets: [
      {
        match: { collection: 'posts' },
        pattern: 'blog/:date/:title',
        date: 'mmddyy'
      },
      {
        match: { collection: 'pages' },
        pattern: 'pages/:title'
      }
    ]
  }))
  .use(layouts(               // wrap layouts around html
      {
        pattern: "**/*.html",
        default: "default.hbs",
        supressNoFilesError: true
      }
  ))
  .use(sass())
  .use(copy(
    {
        pattern: "static/*",
        directory: "static"
    }
  ))
  .use(copy(
    {
        pattern: ".well-known/*",
        directory: "well-known"
    }
  ))

site.build(function(err){
  console.log(err)
})